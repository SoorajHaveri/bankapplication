package com.learn.demobank.entity;

import java.time.LocalDateTime;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.CreationTimestamp;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerRegistration {
	
	@Id
	private String customerId;
	private String firstName;
	private String lastName;
	private String contactNo;
	private String aadharNo;
	private String password;
	@OneToOne
	@Cascade(CascadeType.ALL)
	private Address address;
	private boolean loggedIn;
	@CreationTimestamp
	private LocalDateTime registeredAt;
	

}