package com.learn.demobank.exception;

public class InsufficientFundException extends BankAppGlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InsufficientFundException() {
		super("Insufficent funds", GlobalErrorCode.ERROR_INSUFFICIENT_FUND);

	}

	public InsufficientFundException(String message) {
		super(message, GlobalErrorCode.ERROR_INSUFFICIENT_FUND);

	}

}
