package com.learn.demobank.exception;

import org.springframework.http.HttpStatus;

public class GlobalErrorCode {
	public static final HttpStatus ERROR_RESOURCE_NOT_FOUND = HttpStatus.NOT_FOUND;
	public static final HttpStatus ERROR_RESOURCE_CONFLICT_EXISTS = HttpStatus.CONFLICT;
	public static final HttpStatus ERROR_UNAUTHOURIZED_USER = HttpStatus.UNAUTHORIZED;
	public static final HttpStatus ERROR_INSUFFICIENT_FUND = HttpStatus.BAD_REQUEST;

	private GlobalErrorCode() {
		super();
	}
}