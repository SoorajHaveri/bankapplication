package com.learn.demobank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.learn.demobank.dto.ApiResponse;
import com.learn.demobank.dto.FundTransactionDto;
import com.learn.demobank.entity.FundTransaction;

public interface FundTransactionRepository extends JpaRepository<FundTransaction, Long>{

	

}
