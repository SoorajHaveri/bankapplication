package com.learn.demobank.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.learn.demobank.entity.Accountdetails;
import com.learn.demobank.entity.CustomerRegistration;
import java.util.List;


public interface AccountRepository extends JpaRepository<Accountdetails, String> {

	Optional<Accountdetails> findById(String accountId);
	
	Optional<Accountdetails> findByAccountNumberAndCustomer(long fromAccount, CustomerRegistration customer);
	
	Optional<Accountdetails>  findByAccountNumber(long accountNumber);
}
