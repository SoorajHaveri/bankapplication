package com.learn.demobank.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.learn.demobank.entity.CustomerRegistration;
import java.util.List;


public interface CustomerRepository extends JpaRepository<CustomerRegistration, String> {
	Optional<CustomerRegistration>  findByAadharNo(String aadharNo);
	
	Optional<CustomerRegistration>  findByCustomerId(String customerId);
}
