package com.learn.demobank.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.learn.demobank.entity.BenificiaryAccount;
import com.learn.demobank.entity.CustomerRegistration;

public interface BenificiaryAccountRepository extends JpaRepository<BenificiaryAccount, Long> {
	Optional<BenificiaryAccount> findByAccountNumberAndCustomer(long accountNumber, CustomerRegistration customer);
}
