package com.learn.demobank.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FundTransactionDto {
	@NotNull(message = "fromAccount is required")
	private long fromAccount;
	@NotNull(message = "toAccount is required")
	private long toAccount;
	@NotNull(message = "amount is required")
	@Min(value = 1, message = "minimum amount must be 1")
	@Max(value = 10000, message = "maximum is 10000")
	private double amount;

}
