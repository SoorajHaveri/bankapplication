package com.learn.demobank.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressDto {
	@NotBlank(message = "city required")
	private String city;
	@NotBlank(message = "state required")
	private String state;
	@NotBlank(message = "country required")
	private String country;
	@NotNull(message = "pincode is required")
	private long pincode;

}
