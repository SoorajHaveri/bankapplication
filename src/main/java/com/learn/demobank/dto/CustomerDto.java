package com.learn.demobank.dto;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerDto {
	@NotBlank(message = "firstName required")
	private String firstName;
	private String lastName;
	@NotBlank(message = "phone number is required")
	@Pattern(regexp = "[6-9][\\d]{9}", message = "invalid phone number")
	private String phone;
	@NotBlank(message = "aadhar number is required")
	@Pattern(regexp = "[\\d]{12}", message = "invalid aadhar")
	private String aadhar;
	@NotNull(message = "initial deposit is required")
	@Min(value = 500, message = "initial deposit must equals or greater to 500")
	private double initialDeposit;
	@Valid
	private AddressDto addressDto;

}

