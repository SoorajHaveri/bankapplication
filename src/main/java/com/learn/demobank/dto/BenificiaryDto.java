package com.learn.demobank.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BenificiaryDto {
	@NotNull(message = "accountNumber is required")
//	@Size(min = 10, max = 14 , message="Give account number is min-10 digits and max-14")
	private long accountNumber;
	@NotBlank(message = "ifsc is required")
	private String ifsc;
	@NotBlank(message = "bankName is required")
//	@Size(min = 3, max = 25 , message="Bank name should be between 3 character to 25 character")
	private String bankName;
	@NotBlank(message = "accountHolderName is required")
//	@Size(min = 5, max = 25 , message="AccountHolderNameshould be between 5 character to 25 character")
	private String accountHolderName;

}
