package com.learn.demobank.service;

import com.learn.demobank.dto.AccountdetailsDto;
import com.learn.demobank.dto.ApiResponse;

public interface AccountService {

	ApiResponse update(String customerId, AccountdetailsDto accountdetailsDto);

}
