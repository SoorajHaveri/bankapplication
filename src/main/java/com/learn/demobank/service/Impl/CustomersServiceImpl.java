package com.learn.demobank.service.Impl;

import java.util.Optional;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.learn.demobank.dto.AddressDto;
import com.learn.demobank.dto.ApiResponse;
import com.learn.demobank.dto.BenificiaryDto;
import com.learn.demobank.dto.CustomerDto;
import com.learn.demobank.dto.FundTransactionDto;
import com.learn.demobank.entity.Accountdetails;
import com.learn.demobank.entity.Address;
import com.learn.demobank.entity.BenificiaryAccount;
import com.learn.demobank.entity.CustomerRegistration;
import com.learn.demobank.entity.FundTransaction;
import com.learn.demobank.exception.InsufficientFundException;
import com.learn.demobank.exception.ResourceConflictExists;
import com.learn.demobank.exception.ResourceNotFound;
import com.learn.demobank.exception.UnauthorizedUser;
import com.learn.demobank.repository.AccountRepository;
import com.learn.demobank.repository.BenificiaryAccountRepository;
import com.learn.demobank.repository.CustomerRepository;
import com.learn.demobank.repository.FundTransactionRepository;
import com.learn.demobank.service.CustomersService;

@Service
public class CustomersServiceImpl implements CustomersService {

	private final CustomerRepository customerRepository;
	private final AccountRepository accountRepository;
	private final BenificiaryAccountRepository benificiaryAccountRepository;
	private final FundTransactionRepository fundTransactionRepository;

	@Autowired
	public CustomersServiceImpl(CustomerRepository customerRepository, AccountRepository accountRepository,
			BenificiaryAccountRepository benificiaryAccountRepository,FundTransactionRepository fundTransactionRepository) {
		super();
		this.customerRepository = customerRepository;
		this.accountRepository = accountRepository;
		this.benificiaryAccountRepository = benificiaryAccountRepository;
		this.fundTransactionRepository = fundTransactionRepository;
	}

	@Override
	public ApiResponse register(CustomerDto customerDto) {
		Optional<CustomerRegistration> aadharNo = customerRepository.findByAadharNo(customerDto.getAadhar());
		if (aadharNo.isPresent()) {
			throw new ResourceConflictExists("Customer already register");
		}
		String characters1 = "0123456789";
		String customId1 = RandomStringUtils.random(7, characters1);
		String customId2 = "SBI" + customId1 + "AB";

		String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()";
		String pwd = RandomStringUtils.random(8, characters);

		CustomerRegistration customer = new CustomerRegistration();
		customer.setCustomerId(customId2);
		customer.setFirstName(customerDto.getFirstName());
		customer.setLastName(customerDto.getLastName());
		customer.setContactNo(customerDto.getPhone());
		customer.setPassword(pwd);
		customer.setAadharNo(customerDto.getAadhar());
		customer.setAddress(getAddress(customerDto.getAddressDto()));

		customerRepository.save(customer);

		String characters2 = "0123456789";
		String acc1 = RandomStringUtils.random(11, characters2);
		long accountNumber = Long.parseLong(acc1);

		Accountdetails account = new Accountdetails();
		account.setBankName("State Bank Of Mysore");
		account.setIfsc("SBI0000123");
		account.setAccountNumber(accountNumber);
		account.setBalance(customerDto.getInitialDeposit());
		account.setAccountId(customId2);
		account.setCustomer(customer);
		accountRepository.save(account);

		ApiResponse api = new ApiResponse();
		api.setHttpCode(201l);
		api.setMessage("Your Registration is Successfully");

		return api;
	}

	private Address getAddress(AddressDto addressDto) {
		Address address = new Address();
		address.setCity(addressDto.getCity());
		address.setState(addressDto.getState());
		address.setCountry(addressDto.getCountry());
		address.setPincode(addressDto.getPincode());
		return address;
	}

	@Override
	public ApiResponse login(String customerId, String password) {
		CustomerRegistration customer = customerRepository.findById(customerId)
				.orElseThrow(() -> new ResourceNotFound("Invalid customerId"));
		if (customer.getPassword().equals(password)) {
			customer.setLoggedIn(true);
			customerRepository.save(customer);
			ApiResponse api = new ApiResponse();
			api.setHttpCode(201l);
			api.setMessage("Logged-in Successfully");
			return api;
		}else
			throw new UnauthorizedUser("Password does not match");
		
	}

	@Override
	public ApiResponse addBenificiary(String customerId, BenificiaryDto benificiaryDto) {
		Optional<CustomerRegistration> customer = customerRepository.findByCustomerId(customerId);
		if (customer.isEmpty()) {
			throw new ResourceNotFound("Customer id not exists");
		}
		CustomerRegistration customer1 = customer.get();
		if (customer.isPresent()) {
			boolean status = customer1.isLoggedIn();
			if (status) {
				Optional<BenificiaryAccount> account = benificiaryAccountRepository
						.findByAccountNumberAndCustomer(benificiaryDto.getAccountNumber(), customer1);
				if (account.isPresent()) {
					throw new ResourceNotFound("Benificiary account already present");
				}
				BenificiaryAccount benificiaryAccount = new BenificiaryAccount();
				benificiaryAccount.setAccountHolderName(benificiaryDto.getAccountHolderName());
				benificiaryAccount.setBankName(benificiaryDto.getBankName());
				benificiaryAccount.setAccountNumber(benificiaryDto.getAccountNumber());
				benificiaryAccount.setIfsc(benificiaryDto.getIfsc());
				benificiaryAccount.setCustomer(customer1);
				benificiaryAccountRepository.save(benificiaryAccount);

				ApiResponse api = new ApiResponse();
				api.setHttpCode(201l);
				api.setMessage("Benificiary Account added");
				return api;
			} else
				throw new UnauthorizedUser("Login to add Benificiary Details");

		} else
			throw new UnauthorizedUser("Unauthorized Customer");

	}

	@Override
	public ApiResponse fundTransfer(String customerId, FundTransactionDto fundTransactionDto) {
		CustomerRegistration customer = customerRepository.findById(customerId)
				.orElseThrow(() -> new ResourceNotFound("Customer Not found with CustomerID "));
		if (customer.isLoggedIn()) {
			Accountdetails accountNumber = accountRepository
					.findByAccountNumberAndCustomer(fundTransactionDto.getFromAccount(), customer)
					.orElseThrow(() -> new ResourceNotFound("Account Number NotFound"));

			BenificiaryAccount benificiaryAccount = benificiaryAccountRepository
					.findByAccountNumberAndCustomer(fundTransactionDto.getToAccount(), customer)
					.orElseThrow(() -> new ResourceNotFound("Benificiary Account Number NotFound"));

			if (accountNumber.getBalance() >= fundTransactionDto.getAmount()) {
				accountNumber.setBalance(accountNumber.getBalance() - fundTransactionDto.getAmount());
				accountRepository.save(accountNumber);
				
				Optional<Accountdetails> number = accountRepository.findByAccountNumber(fundTransactionDto.getToAccount());
				Accountdetails accountdetails = number.get();
				accountdetails.setBalance(accountdetails.getBalance()+ fundTransactionDto.getAmount());
				accountRepository.save(accountdetails);

				FundTransaction transaction = new FundTransaction();
				transaction.setFromAccount(fundTransactionDto.getFromAccount());
				transaction.setToAccount(fundTransactionDto.getToAccount());
				transaction.setAmount(fundTransactionDto.getAmount());
				fundTransactionRepository.save(transaction);
			} else {
				throw new InsufficientFundException("Insufficient Funds in Source account");
			}

		} else {
			throw new UnauthorizedUser("Unauthorized(customer not logged in)");
		}

		ApiResponse api = new ApiResponse();
		api.setHttpCode(201l);
		api.setMessage("Transaction succes");
		return api;
	}

}
