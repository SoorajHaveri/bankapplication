package com.learn.demobank.service.Impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.learn.demobank.dto.AccountdetailsDto;
import com.learn.demobank.dto.ApiResponse;
import com.learn.demobank.entity.Accountdetails;
import com.learn.demobank.entity.CustomerRegistration;

import com.learn.demobank.exception.ResourceNotFound;
import com.learn.demobank.exception.UnauthorizedUser;

import com.learn.demobank.repository.AccountRepository;
import com.learn.demobank.repository.CustomerRepository;
import com.learn.demobank.service.AccountService;

@Service
public class AccountServiceImpl implements AccountService {

	private final AccountRepository accountRepository;
	private final CustomerRepository customerRepository;

	@Autowired
	public AccountServiceImpl(AccountRepository accountRepository, CustomerRepository customerRepository) {
		super();
		this.accountRepository = accountRepository;
		this.customerRepository = customerRepository;
	}

	@Override
	public ApiResponse update(String customerId, AccountdetailsDto accountdetailsDto) {
		Optional<CustomerRegistration> customer = customerRepository.findByCustomerId(customerId);
		Optional<Accountdetails> byId = accountRepository.findById(customerId);

		if (customer.isEmpty()) {
			throw new ResourceNotFound("CustomerID is not Exits");
		}
		CustomerRegistration customer1 = customer.get();
		if (customer.isPresent()) {
			boolean status = customer1.isLoggedIn();
			if (status) {
				Accountdetails account = byId.get();
				account.setAccountType(accountdetailsDto.getAccountType());
				double amount = account.getBalance();
				account.setBalance(accountdetailsDto.getBalance() + amount);
				accountRepository.save(account);

				ApiResponse api = new ApiResponse();
				api.setHttpCode(200l);
				api.setMessage("Account update sucessfully...");

				return api;
			} else
				throw new UnauthorizedUser("Login to update the Account");//

		} else
			throw new UnauthorizedUser("Unauthorized Customer");

	}

}
