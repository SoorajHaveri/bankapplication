package com.learn.demobank.service;

import com.learn.demobank.dto.ApiResponse;
import com.learn.demobank.dto.BenificiaryDto;
import com.learn.demobank.dto.CustomerDto;
import com.learn.demobank.dto.FundTransactionDto;

import jakarta.validation.Valid;

public interface CustomersService {

	ApiResponse register(CustomerDto customerDto);

	ApiResponse login(String customerId, String password);

	ApiResponse addBenificiary(String customerId, BenificiaryDto benificiaryDto);

	ApiResponse fundTransfer(String customerId, FundTransactionDto fundTransactionDto);

}
