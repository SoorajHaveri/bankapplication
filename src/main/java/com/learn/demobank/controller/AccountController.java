package com.learn.demobank.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.learn.demobank.dto.AccountdetailsDto;
import com.learn.demobank.dto.ApiResponse;
import com.learn.demobank.service.AccountService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/account")
public class AccountController {
	
	private final AccountService accountService;
	
	@Autowired
	public AccountController(AccountService accountService) {
		super();
		this.accountService = accountService;
	}
	
	@PutMapping("/{customerId}")
	public ResponseEntity<ApiResponse> updateAccount(@PathVariable String customerId, @Valid @RequestBody AccountdetailsDto accountdetailsDto){
		return new ResponseEntity<ApiResponse>(accountService.update(customerId,accountdetailsDto),HttpStatus.OK);
		
	}
	
	

}
