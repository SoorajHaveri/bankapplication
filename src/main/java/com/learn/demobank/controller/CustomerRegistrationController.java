package com.learn.demobank.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.learn.demobank.dto.ApiResponse;
import com.learn.demobank.dto.CustomerDto;
import com.learn.demobank.service.CustomersService;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;

@RestController
@RequestMapping("/customer")
public class CustomerRegistrationController {

	private final CustomersService customerService;

	@Autowired
	public CustomerRegistrationController(CustomersService customerService) {
		super();
		this.customerService = customerService;
	}

	@PostMapping("/register")
	public ResponseEntity<ApiResponse> customerRegister(@Valid @RequestBody CustomerDto customerDto) {
		return new ResponseEntity<ApiResponse>(customerService.register(customerDto), HttpStatus.CREATED);
	}

	@PostMapping("/login")
	public ResponseEntity<ApiResponse> login(@Valid @NotBlank @RequestParam String customerId,
			@RequestParam String password) {

		return new ResponseEntity<ApiResponse>(customerService.login(customerId, password), HttpStatus.OK);

	}
	
//	@RestController
//	@RequestMapping("/customers")
//	@RequiredArgsConstructor
//	public class BenificiaryController {
//		private final CustomerService customerService;
//
//		@PostMapping("/benificiary-account/{customerId}")
//		public ResponseEntity<ApiResponse> addBenificiary(@PathVariable String customerId,
//				@Valid @RequestBody BenificiaryDto benificiaryDto) {
//			return ResponseEntity.status(HttpStatus.CREATED)
//					.body(customerService.addBeneficiary(customerId, benificiaryDto));
//		}


}
