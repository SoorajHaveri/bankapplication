package com.learn.demobank.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.learn.demobank.dto.ApiResponse;
import com.learn.demobank.dto.BenificiaryDto;
import com.learn.demobank.dto.FundTransactionDto;
import com.learn.demobank.service.CustomersService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("\benificiary-account")
public class BenificiaryController 
{
	
	private final CustomersService  customerService;
	
	@Autowired
	public BenificiaryController(CustomersService customerService) {
		super();
		this.customerService = customerService;
	}
	@PostMapping("/{customerId}")
	public ResponseEntity<ApiResponse> addbenificiary(@PathVariable String customerId,
			@Valid @RequestBody BenificiaryDto benificiaryDto)
	{
		return new  ResponseEntity<ApiResponse>(customerService.addBenificiary(customerId, benificiaryDto),HttpStatus.CREATED);
		
	}
	  
	@PostMapping("/transaction/{customerId}")
	public ResponseEntity<ApiResponse> fundTransfer(@PathVariable String customerId,
			@Valid @RequestBody FundTransactionDto fundTransactionDto) {
		return new ResponseEntity<ApiResponse>(customerService.fundTransfer(customerId,fundTransactionDto),HttpStatus.CREATED);
		
	}

	}

