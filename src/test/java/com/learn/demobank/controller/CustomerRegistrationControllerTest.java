package com.learn.demobank.controller;
 
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
 
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
 
import com.learn.demobank.dto.AddressDto;
import com.learn.demobank.dto.ApiResponse;
import com.learn.demobank.dto.CustomerDto;
import com.learn.demobank.service.CustomersService;
 
@ExtendWith(SpringExtension.class)
class CustomerRegistrationControllerTest {
 
	@InjectMocks
	private CustomerRegistrationController customerRegistrationController;
 
	@Mock
	private CustomersService customersService;
 
	@Test
	void testRegisterSuccess() {
		AddressDto add = new AddressDto();
		add.setCity("Mysore");
		add.setState("KA");
		add.setCountry("India");
		add.setPincode(571107);
 
		CustomerDto customer = new CustomerDto();
		customer.setFirstName("Praveen");
		customer.setLastName("kumar");
		customer.setPhone("9535053345");
		customer.setAadhar("123456789012");
		customer.setAddressDto(add);
 
		ApiResponse api = new ApiResponse();
		api.setHttpCode(201l);
 
		Mockito.when(customersService.register(customer)).thenReturn(api);
		ResponseEntity<ApiResponse> customerRegister = customerRegistrationController.customerRegister(customer);
		assertEquals(201, customerRegister.getBody().getHttpCode());
	}
	@Test
	void testRegisterUnSuccess() {
		AddressDto add = new AddressDto();
		add.setCity("Mysore");
		add.setState("KA");
		add.setCountry("India");
		add.setPincode(571107);
 
		CustomerDto customer = new CustomerDto();
		customer.setFirstName("Praveen");
		customer.setLastName("kumar");
		customer.setPhone("9535053345");
		customer.setAadhar("123456789012");
		customer.setAddressDto(add);
 
		ApiResponse api = new ApiResponse();
		api.setHttpCode(201l);
 
		Mockito.when(customersService.register(customer)).thenReturn(api);
		ResponseEntity<ApiResponse> customerRegister = customerRegistrationController.customerRegister(customer);
		assertNotEquals(202, customerRegister.getBody().getHttpCode());
	}
 
 
}
 