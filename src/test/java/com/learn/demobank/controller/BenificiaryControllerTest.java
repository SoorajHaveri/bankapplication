package com.learn.demobank.controller;
 
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
 
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
 
import com.learn.demobank.dto.ApiResponse;
import com.learn.demobank.dto.BenificiaryDto;
import com.learn.demobank.dto.FundTransactionDto;
import com.learn.demobank.service.CustomersService;
 
@ExtendWith(SpringExtension.class)
class BenificiaryControllerTest {
	
	@InjectMocks
	private BenificiaryController benificiaryController;
	@Mock
	private CustomersService customersService;
	
	@Test
	void testBenificiaryAddSuccess() {
		String cId= "SBI2081917AB";
		BenificiaryDto benificiary = new BenificiaryDto();
		benificiary.setAccountHolderName("Praveen");
		benificiary.setAccountNumber(12331231l);
		benificiary.setBankName("SBM BANK");
		benificiary.setIfsc("SBI0000123");
 
		ApiResponse api = new ApiResponse();
		api.setHttpCode(201l);
 
		Mockito.when(customersService.addBenificiary(cId, benificiary)).thenReturn(api);
		ResponseEntity<ApiResponse> benificiary2 = benificiaryController.addbenificiary(cId, benificiary);
		assertEquals(201, benificiary2.getBody().getHttpCode());
	}
	@Test
	void testBenificiaryAddUnSuccess() {
		String cId= "SBI2081917AB";
		
		BenificiaryDto benificiary = new BenificiaryDto();
		benificiary.setAccountHolderName("Praveen");
		benificiary.setAccountNumber(12331231l);
		benificiary.setBankName("SBM BANK");
		benificiary.setIfsc("SBI0000123");
 
		ApiResponse api = new ApiResponse();
		api.setHttpCode(201l);
 
		Mockito.when(customersService.addBenificiary(cId, benificiary)).thenReturn(api);
		ResponseEntity<ApiResponse> benificiary2 = benificiaryController.addbenificiary(cId, benificiary);
		assertNotEquals(202, benificiary2.getBody().getHttpCode());
	}
	@Test
	void testFundTransferSuccess() {
		String cId= "SBI2081917AB";
		
		FundTransactionDto fund = new FundTransactionDto();
		fund.setAmount(1000);
		fund.setFromAccount(12345678l);
		fund.setToAccount(4567893444l);
		
		ApiResponse api = new ApiResponse();
		api.setHttpCode(201l);
		
		Mockito.when(customersService.fundTransfer(cId, fund)).thenReturn(api);
		ResponseEntity<ApiResponse> fundTransfer = benificiaryController.fundTransfer(cId, fund);
		assertEquals(201, fundTransfer.getBody().getHttpCode());
		
	}
	@Test
	void testFundTransferUnSuccess() {
		String cId= "SBI2081917AB";
		
		FundTransactionDto fund = new FundTransactionDto();
		fund.setAmount(1000);
		fund.setFromAccount(12345678l);
		fund.setToAccount(4567893444l);
		
		ApiResponse api = new ApiResponse();
		api.setHttpCode(201l);
		
		Mockito.when(customersService.fundTransfer(cId, fund)).thenReturn(api);
		ResponseEntity<ApiResponse> fundTransfer = benificiaryController.fundTransfer(cId, fund);
		assertNotEquals(202, fundTransfer.getBody().getHttpCode());
		
	}
	
	
 
}