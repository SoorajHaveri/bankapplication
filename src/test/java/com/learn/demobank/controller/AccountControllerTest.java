package com.learn.demobank.controller;
 
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
 
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
 
import com.learn.demobank.dto.AccountdetailsDto;
import com.learn.demobank.dto.ApiResponse;
import com.learn.demobank.service.AccountService;
 
@ExtendWith(SpringExtension.class)
class AccountControllerTest {
 
	@InjectMocks
	private AccountController accountController;
	@Mock
	private AccountService accountService;
 
	@Test
	void testUpdateAccountSuccess() {
		String cId = "SBI2081917AB";
 
		AccountdetailsDto account = new AccountdetailsDto();
		account.setAccountType("Saving");
		account.setBalance(100);
 
		ApiResponse api = new ApiResponse();
		api.setHttpCode(201l);
		Mockito.when(accountService.update(cId, account)).thenReturn(api);
		ResponseEntity<ApiResponse> account2 = accountController.updateAccount(cId, account);
		assertEquals(201, account2.getBody().getHttpCode());
	}
 
	@Test
	void testUpdateAccountUnSuccess() {
		String cId = "SBI2081917AB";
 
		AccountdetailsDto account = new AccountdetailsDto();
		account.setAccountType("Saving");
		account.setBalance(100);
 
		ApiResponse api = new ApiResponse();
		api.setHttpCode(201l);
		Mockito.when(accountService.update(cId, account)).thenReturn(api);
		ResponseEntity<ApiResponse> account2 = accountController.updateAccount(cId, account);
		assertNotEquals(202, account2.getBody().getHttpCode());
	}
 
}