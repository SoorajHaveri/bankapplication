package com.learn.demobank.service.Impl;



import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.learn.demobank.dto.AccountdetailsDto;
import com.learn.demobank.dto.ApiResponse;
import com.learn.demobank.entity.Accountdetails;
import com.learn.demobank.entity.CustomerRegistration;
import com.learn.demobank.exception.ResourceNotFound;
import com.learn.demobank.exception.UnauthorizedUser;
import com.learn.demobank.repository.AccountRepository;
import com.learn.demobank.repository.CustomerRepository;

//Test 1: Successful account update when user is logged in 

@ExtendWith(SpringExtension.class)
public class AccountServiceImplTest {
	
	@InjectMocks
	private AccountServiceImpl accountServiceImpl;
	@Mock
	private CustomerRepository customerRepository;
	@Mock
	private AccountRepository accountRepository;

	public void testUpdateAccountSuccess() {
		// Mock dependencies
		CustomerRegistration customer = mock(CustomerRegistration.class);
		Accountdetails accountdetails = mock(Accountdetails.class);
		AccountRepository accountRepository = mock(AccountRepository.class);
		CustomerRepository customerRepository = mock(CustomerRepository.class);

		// Set up mocks
		when(customerRepository.findByCustomerId("customerId")).thenReturn(Optional.of(customer));
		when(customer.isLoggedIn()).thenReturn(true);
		when(accountRepository.findById("customerId")).thenReturn(Optional.of(accountdetails));
		when(accountdetails.getBalance()).thenReturn(100.0);

		// Create service and call method
		AccountServiceImpl accountService = new AccountServiceImpl(accountRepository, customerRepository);
		AccountdetailsDto accountdetailsDto = new AccountdetailsDto("Savings", 50.0);
		ApiResponse apiResponse = accountService.update("customerId", accountdetailsDto);

		// Verify results
		verify(accountRepository).save(accountdetails);
		assertEquals("Account update sucessfully...", apiResponse.getMessage());
	}

//Test 2: Unauthorized access when user is not logged in
	@Test
	public void testUpdateAccountUnauthorized() {
		// Mock dependencies
		CustomerRegistration customer = mock(CustomerRegistration.class);
		Accountdetails accountdetails = mock(Accountdetails.class);
		AccountRepository accountRepository = mock(AccountRepository.class);
		CustomerRepository customerRepository = mock(CustomerRepository.class);

		// Set up mocks
		when(customerRepository.findByCustomerId("customerId")).thenReturn(Optional.of(customer));
		when(customer.isLoggedIn()).thenReturn(false);

		// Create service and call method
		AccountServiceImpl accountService = new AccountServiceImpl(accountRepository, customerRepository);
		AccountdetailsDto accountdetailsDto = new AccountdetailsDto("Savings", 50.0);

		// Expect exception
		assertThrows(UnauthorizedUser.class, () -> accountService.update("customerId", accountdetailsDto));
	}

//Test 3: Resource not found when customer ID is invalid
	@Test
	public void testUpdateAccountResourceNotFound() {
		// Mock dependencies
		AccountRepository accountRepository = mock(AccountRepository.class);
		CustomerRepository customerRepository = mock(CustomerRepository.class);

		// Set up mocks
		when(customerRepository.findByCustomerId("customerId")).thenReturn(Optional.empty());

		// Create service and call method
		AccountServiceImpl accountService = new AccountServiceImpl(accountRepository, customerRepository);
		AccountdetailsDto accountdetailsDto = new AccountdetailsDto("Savings", 50.0);

		// Expect exception
		assertThrows(ResourceNotFound.class, () -> accountService.update("customerId", accountdetailsDto));
	}


	@Test 
	void testUnauthorizedUserUpdate() {
	    // Mock CustomerRepository with customer not logged in
	    CustomerRegistration customer = new CustomerRegistration();
	    customer.setLoggedIn(false);
	    Mockito.when(customerRepository.findByCustomerId(Mockito.anyString())).thenReturn(Optional.of(customer));

	    // Mock AccountRepository
	    Accountdetails account = new Accountdetails();
	    Mockito.when(accountRepository.findById(Mockito.anyString())).thenReturn(Optional.of(account));

	    // Mock AccountdetailsDto
	    AccountdetailsDto accountdetailsDto = new AccountdetailsDto();
	    accountdetailsDto.setAccountType("Savings");
	    accountdetailsDto.setBalance(1000.0);

	    // Call the method and assert that it throws UnauthorizedUser exception
	    assertThrows(UnauthorizedUser.class, () -> {
	        accountServiceImpl.update("customerId", accountdetailsDto);
	    });
	}
	@Test 
	void testAuthorizedUserUpdate() {
	    // Mock CustomerRepository with customer logged in
	    CustomerRegistration customer = new CustomerRegistration();
	    customer.setLoggedIn(true);
	    Mockito.when(customerRepository.findByCustomerId(Mockito.anyString())).thenReturn(Optional.of(customer));

	    // Mock AccountRepository
	    Accountdetails account = new Accountdetails();
	    Mockito.when(accountRepository.findById(Mockito.anyString())).thenReturn(Optional.of(account));

	    // Mock AccountdetailsDto
	    AccountdetailsDto accountdetailsDto = new AccountdetailsDto();
	    accountdetailsDto.setAccountType("Savings");
	    accountdetailsDto.setBalance(1000.0);

	    // Call the method and verify that it updates the account
	    ApiResponse response = accountServiceImpl.update("customerId", accountdetailsDto);

	    // Assertions
	   // assertEquals(200L, response.getHttpCode());
	    assertEquals(200L, response.getHttpCode());
	    assertEquals("Account update sucessfully...", response.getMessage());
	}


	
}
