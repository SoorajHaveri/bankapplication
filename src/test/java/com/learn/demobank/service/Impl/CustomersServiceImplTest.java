package com.learn.demobank.service.Impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.learn.demobank.dto.AddressDto;
import com.learn.demobank.dto.ApiResponse;
import com.learn.demobank.dto.BenificiaryDto;
import com.learn.demobank.dto.CustomerDto;
import com.learn.demobank.dto.FundTransactionDto;
import com.learn.demobank.entity.Accountdetails;
import com.learn.demobank.entity.BenificiaryAccount;
import com.learn.demobank.entity.CustomerRegistration;
import com.learn.demobank.exception.ResourceConflictExists;
import com.learn.demobank.exception.ResourceNotFound;
import com.learn.demobank.exception.UnauthorizedUser;
import com.learn.demobank.repository.AccountRepository;
import com.learn.demobank.repository.BenificiaryAccountRepository;
import com.learn.demobank.repository.CustomerRepository;
import com.learn.demobank.repository.FundTransactionRepository;

@ExtendWith(SpringExtension.class)
class CustomersServiceImplTest {

	@InjectMocks
	private CustomersServiceImpl customersServiceImpl;

	@Mock
	private CustomerRepository customerRepository;
	@Mock
	private AccountRepository accountRepository;
	@Mock
	private BenificiaryAccountRepository benificiaryAccountRepository;
	
	@Mock 
	private FundTransactionRepository  fundTransactionRepository;

	@Test
	void customerRegisteredAlreadyExist() {
		CustomerRegistration customer = new CustomerRegistration();
		customer.setAadharNo("1234567879012");

		Mockito.when(customerRepository.findByAadharNo("1234567879012")).thenReturn(Optional.of(customer));
		CustomerDto customerDto = new CustomerDto();
		customerDto.setAadhar("1234567879012");
		assertThrows(ResourceConflictExists.class, () -> customersServiceImpl.register(customerDto));
	}

	@Test
	void customerRegisteredsuccess() {

		CustomerRegistration customer = new CustomerRegistration();
		customer.setAadharNo("123456789012");
		Mockito.when(customerRepository.findByAadharNo("123456789012")).thenReturn(Optional.empty());
		Mockito.when(customerRepository.save(customer)).thenReturn(customer);
		
		Accountdetails acc= new Accountdetails();
		acc.setAccountNumber(12233123);
		acc.setAccountType("saving");
		Mockito.when(accountRepository.save(acc)).thenReturn(acc);

		AddressDto add = new AddressDto();
		add.setCity("Mysore");
		add.setCountry("India");
		add.setState("KA");
		add.setPincode(121121);

		CustomerDto cdto = new CustomerDto();
		cdto.setAadhar("123456789012");
		cdto.setAddressDto(add);
		ApiResponse register = customersServiceImpl.register(cdto);
		assertEquals("Your Registration is Successfully", register.getMessage());
	}

	@Test
	void loginSuccess() {
		String customerId = "testCustomer";
		String password = "testPassword";
		CustomerRegistration mockCustomer = new CustomerRegistration();
		mockCustomer.setCustomerId(customerId);
		mockCustomer.setPassword(password);

		Mockito.when(customerRepository.findById(customerId)).thenReturn(Optional.of(mockCustomer));

		ApiResponse response = customersServiceImpl.login(customerId, password);

		assertEquals("Logged-in Successfully", response.getMessage());
	}

	@Test
	void loginInvalidCustomerId() {
		// Arrange
		String customerId = "invalidCustomer";
		String password = "testPassword";
		Mockito.when(customerRepository.findById(customerId)).thenReturn(Optional.empty());

		// Act & Assert
		assertThrows(ResourceNotFound.class, () -> customersServiceImpl.login(customerId, password));

	}
	@Test
    void testLogin_IncorrectPassword() {
        // Arrange
        String customerId = "testCustomer";
        String correctPassword = "correctPassword";
        String incorrectPassword = "incorrectPassword";
        
        CustomerRegistration mockCustomer = new CustomerRegistration();
        mockCustomer.setCustomerId(customerId);
        mockCustomer.setPassword(correctPassword);
        Mockito.when(customerRepository.findById(customerId)).thenReturn(Optional.of(mockCustomer));
        
        ApiResponse response = customersServiceImpl.login(customerId, incorrectPassword);
        assertNotEquals("Wrong password", response.getMessage());
	}
	@Test
	void addBenificiarySuccess() {
		String cId="SBI2081917AB";
		CustomerRegistration custom = new CustomerRegistration();
		custom.setCustomerId(cId);
		custom.setLoggedIn(true);
		Mockito.when(customerRepository.findByCustomerId(cId)).thenReturn(Optional.of(custom));
		BenificiaryAccount benificiary= new BenificiaryAccount();
		benificiary.setAccountHolderName("Praveen");
		benificiary.setAccountNumber(123135625163l);
//		benificiary.setCustomer(custom);
		Mockito.when(benificiaryAccountRepository.findByAccountNumberAndCustomer(123135625163l, custom)).thenReturn(Optional.empty());
		Mockito.when(benificiaryAccountRepository.save(benificiary)).thenReturn(benificiary);
		
		BenificiaryDto benificiaryDto= new BenificiaryDto();
		benificiaryDto.setAccountHolderName("Praveen");
		benificiaryDto.setAccountNumber(123135625163l);
		
		ApiResponse register = customersServiceImpl.addBenificiary(cId, benificiaryDto);
		assertEquals("Benificiary Account added", register.getMessage());
		
		
	}
	
	@Test
		public void benificiaryAlreadyExist() {
			String cId="SBI2081917AB";
			
			CustomerRegistration custom = new CustomerRegistration();
			custom.setCustomerId(cId);
			custom.setLoggedIn(true);
			Mockito.when(customerRepository.findByCustomerId("SBI2081917AB")).thenReturn(Optional.of(custom));
			
			BenificiaryAccount benificiary= new BenificiaryAccount();
			benificiary.setAccountHolderName("Praveen");
			benificiary.setAccountNumber(123135625163l);
			benificiary.setCustomer(custom);
			
			Mockito.when(benificiaryAccountRepository.findByAccountNumberAndCustomer(123135625163l, custom)).thenReturn(Optional.of(benificiary));		
			BenificiaryDto benificiaryDto= new BenificiaryDto();
			benificiaryDto.setAccountHolderName("Praveen");
			benificiaryDto.setAccountNumber(123135625163l);
			assertThrows(ResourceNotFound.class, ()-> customersServiceImpl.addBenificiary(cId, benificiaryDto));
		}
	
	@Test 
	void testFundTransferSuccess() {
	    // Mock CustomerRepository
	    CustomerRegistration customer = new CustomerRegistration();
	    customer.setLoggedIn(true);
	    Mockito.when(customerRepository.findById(Mockito.anyString())).thenReturn(Optional.of(customer));

	    // Mock AccountRepository
	    Accountdetails accountNumber = new Accountdetails();
	    accountNumber.setBalance(1000);
	    Mockito.when(accountRepository.findByAccountNumberAndCustomer(Mockito.anyLong(), Mockito.any())).thenReturn(Optional.of(accountNumber));

	    // Mock BenificiaryAccountRepository
	    BenificiaryAccount benificiaryAccount = new BenificiaryAccount();
	    Mockito.when(benificiaryAccountRepository.findByAccountNumberAndCustomer(Mockito.anyLong(), Mockito.any())).thenReturn(Optional.of(benificiaryAccount));

	    // Mock FundTransactionDto
	    FundTransactionDto fundTransactionDto = new FundTransactionDto();
	    fundTransactionDto.setAmount(500);
	    fundTransactionDto.setFromAccount(1234567890L);
	    fundTransactionDto.setToAccount(9876543210L);

	    // Call the method
	    ApiResponse apiResponse = customersServiceImpl.fundTransfer("customerId", fundTransactionDto);

	    // Assertions
	    assertEquals(201L, apiResponse.getHttpCode());
	    assertEquals("Transaction succes", apiResponse.getMessage());
	}

	@Test 
	void testFundTransferInsufficientFunds() {
	    // Mock CustomerRepository
	    CustomerRegistration customer = new CustomerRegistration();
	    customer.setLoggedIn(true);
	    Mockito.when(customerRepository.findById(Mockito.anyString())).thenReturn(Optional.of(customer));

	    // Mock AccountRepository with insufficient balance
	    Accountdetails accountNumber = new Accountdetails();
	    accountNumber.setBalance(100);
	    Mockito.when(accountRepository.findByAccountNumberAndCustomer(Mockito.anyLong(), Mockito.any())).thenReturn(Optional.of(accountNumber));

	    // Mock FundTransactionDto
	    FundTransactionDto fundTransactionDto = new FundTransactionDto();
	    fundTransactionDto.setAmount(500);
	    fundTransactionDto.setFromAccount(1234567890L);
	    fundTransactionDto.setToAccount(9876543210L);

	    // Call the method and assert that it throws InsufficientFundException
	    assertThrows(ResourceNotFound.class, () -> {
	    	customersServiceImpl.fundTransfer("customerId", fundTransactionDto);
	    });
	}

	@Test 
	void testFundTransferUnauthorizedUser() {
	    // Mock CustomerRepository with customer not logged in
	    CustomerRegistration customer = new CustomerRegistration();
	    customer.setLoggedIn(false);
	    Mockito.when(customerRepository.findById(Mockito.anyString())).thenReturn(Optional.of(customer));

	    // Mock FundTransactionDto
	    FundTransactionDto fundTransactionDto = new FundTransactionDto();
	    fundTransactionDto.setAmount(500);
	    fundTransactionDto.setFromAccount(1234567890L);
	    fundTransactionDto.setToAccount(9876543210L);

	    // Call the method and assert that it throws UnauthorizedUser exception
	    assertThrows(UnauthorizedUser.class, () -> {
	    	customersServiceImpl.fundTransfer("customerId", fundTransactionDto);
	    });
	}



}
